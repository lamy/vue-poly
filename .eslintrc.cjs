/* eslint-env node */
require('@rushstack/eslint-patch/modern-module-resolution')

module.exports = {
  root: true,
  extends: [
    'plugin:vue/vue3-essential',
    'eslint:recommended',
    '@vue/eslint-config-typescript',
    '@vue/eslint-config-prettier/skip-formatting',
  ],
  parserOptions: {
    ecmaVersion: 'latest',
  },
  rules: {
    eqeqeq: 'error',
    'no-await-in-loop': 'error',
    'no-invalid-this': 'error',
    'no-return-assign': 'error',
    'no-unused-expressions': [
      'error',
      {
        allowTernary: true,
      },
    ],
    'no-useless-concat': 'error',
    'no-useless-return': 'error',
    'no-constant-condition': 'error',
    'no-unused-vars': [
      'error',
      {
        argsIgnorePattern: '^_',
      },
    ],
    'no-console': 'error',
    'no-undef': 'error',
    'no-eval': 'error',
    'no-implied-eval': 'error',
    'no-debugger': 'error',
    'prefer-promise-reject-errors': 'error',
    'no-confusing-arrow': 'error',
  },
  overrides: [
    {
      files: ['*.mjs', '*.cjs', 'vite-lib.config.ts'],
      parser: '@babel/eslint-parser',
      parserOptions: {
        requireConfigFile: false,
      },
      env: {
        node: true,
      },
    },
  ],
}
