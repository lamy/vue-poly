// eslint-disable-next-line @typescript-eslint/no-var-requires
const cssPropertyOrder = require('./configs/stylelint/css-order.cjs')

module.exports = {
  extends: ['stylelint-config-standard-scss', 'stylelint-config-standard-vue/scss'],
  plugins: ['stylelint-order'],
  rules: {
    // Allow newlines inside class attribute values
    'string-no-newline': null,
    'font-family-no-duplicate-names': true,
    'order/properties-order': cssPropertyOrder,
    'selector-class-pattern': [
      '^(?:(?:o|c|u|t|s|is|has|_|js|qa)-)?[a-zA-Z0-9]+(?:-[a-zA-Z0-9]+)*(?:__[a-zA-Z0-9]+(?:-[a-zA-Z0-9]+)*)?(?:--[a-zA-Z0-9]+(?:-[a-zA-Z0-9]+)*)?(?:\\[.+\\])?$',
      {
        message: 'Selector should be written in BEM convention (selector-class-pattern)',
      },
    ],
    'selector-id-pattern': /^[a-z][a-zA-Z]*$/,
    // Limit the number of universal selectors in a selector,
    // to avoid very slow selectors
    'selector-max-universal': 1,
    // ===SCSS===
    'scss/dollar-variable-colon-space-after': 'always',
    'scss/dollar-variable-colon-space-before': 'never',
    'scss/dollar-variable-no-missing-interpolation': true,
    'scss/dollar-variable-pattern': /^[a-z][a-z0-9-]+([a-z0-9-]+[a-z0-9]+)?$/, // single words, kebab-case
    'scss/double-slash-comment-whitespace-inside': 'always',
    'scss/operator-no-newline-before': true,
    'scss/operator-no-unspaced': true,
    'scss/selector-no-redundant-nesting-selector': [true, { ignoreKeywords: ['when'] }],
    // Allow SCSS and CSS module keywords beginning with `@`
    'at-rule-no-unknown': null,
    'no-invalid-position-at-import-rule': null,
    'scss/at-rule-no-unknown': true,
    'color-hex-case': null,
  },
}
